import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';

export class PaginationDto<T> {
  @ApiProperty()
  readonly itemCount: number;
  @ApiProperty()
  readonly totalItems: number;
  @ApiProperty()
  readonly pageCount: number;
  @ApiPropertyOptional()
  readonly next?: string;
  @ApiPropertyOptional()
  readonly previous?: string;
}
