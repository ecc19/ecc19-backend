import {ConflictException, Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {UserEntity} from './user.entity';
import {IPaginationOptions, paginate, Pagination} from 'nestjs-typeorm-paginate';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private configService: ConfigService
  ) {

  }

  async onModuleInit(): Promise<void> {
    await this.bootstrapAdmin();
  }

  private async bootstrapAdmin() {
    if (await this.userRepository.count() == 0) {
      const adminUser = new UserEntity();
      adminUser.username = this.configService.get('admin.username');
      adminUser.password = this.configService.get('admin.password');
      adminUser.email = this.configService.get('admin.email');
      adminUser.roles = 'admin';
      return this.userRepository.save(adminUser);
    }
  }

  public findOne(username: string): Promise<UserEntity> {
    return this.userRepository.findOne({username});
  }

  public async create(userData: UserEntity): Promise<UserEntity> {
    const existingUser = await this.userRepository.findOne({email: userData.email, username: userData.username})
    if (existingUser) {
      throw new ConflictException('User already exists');
    }

    return this.userRepository.save(userData);
  }

  public findAll(options: IPaginationOptions): Promise<Pagination<UserEntity>> {
    const qb = this.userRepository
      .createQueryBuilder('user');

    qb.orderBy('user.createdAt', 'DESC');

    return paginate<UserEntity>(qb, options);
  }

  public update(id: number, user: UserEntity): Promise<UserEntity> {
    user.id = id;
    return this.userRepository.save(user);
  }
}
