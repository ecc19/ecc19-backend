import {Injectable, Logger} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {MessageEntity} from './message.entity';
import {IPaginationOptions, paginate, Pagination} from 'nestjs-typeorm-paginate';
import {EmailService} from '../email/email.service';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class MessageService {
  private readonly log = new Logger(MessageService.name);

  constructor(
    @InjectRepository(MessageEntity)
    private messageRepository: Repository<MessageEntity>,
    private emailService: EmailService,
    private configService: ConfigService
  ) {

  }

  public findOne(id: number): Promise<MessageEntity> {
    return this.messageRepository.findOne(id);
  }

  public async create(messageData: MessageEntity): Promise<MessageEntity> {
    await this.messageRepository.save(messageData);
    this.notifyAdminByEmail(messageData);
    return messageData;
  }

  public findAll(options: IPaginationOptions): Promise<Pagination<MessageEntity>> {
    const qb = this.messageRepository
      .createQueryBuilder('message');

    qb.orderBy('message.createdAt', 'DESC');

    return paginate<MessageEntity>(qb, options);
  }

  public update(id: number, message: MessageEntity): Promise<MessageEntity> {
    message.id = id;
    return this.messageRepository.save(message);
  }

  private notifyAdminByEmail(messageData: MessageEntity) {
    this.emailService.sendEmail(
      this.configService.get('admin.email'),
      `Nouveau message de ${messageData.name} - ${messageData.subject}`,
      messageData.content,
      messageData.email
    ).catch(err => {
      this.log.error(err);
    });
  }
}
