import {Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn} from 'typeorm';
import {OfferingEntity} from './offering.entity';
import {FileDataEntity} from '../file/file-data.entity';
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {EquipmentTypeEnum} from '../shared/equipment-type.enum';
import {Exclude} from 'class-transformer';

@Entity('offering_item')
export class OfferingItemEntity {
  @Exclude({toClassOnly: true})
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(type => OfferingEntity, offering => offering.items)
  offering: OfferingEntity;

  @ApiProperty()
  @Column()
  quantity: number;

  @ApiProperty({ enum: EquipmentTypeEnum, enumName: 'EquipmentTypeEnum' })
  @Column('enum', { enum: EquipmentTypeEnum })
  type: EquipmentTypeEnum;

  @ApiPropertyOptional()
  @Column({nullable: true})
  expirationDate: Date;

  @ApiPropertyOptional()
  @Column({nullable: true})
  description: string;

  @ApiPropertyOptional({description: 'Norme'})
  @Column({nullable: true})
  standard: string;

  @ApiPropertyOptional({type: [FileDataEntity]})
  @ManyToMany(type => FileDataEntity)
  @JoinTable()
  photos: FileDataEntity[];

  @ApiProperty()
  @Column()
  postalCode: string;

  @ApiProperty()
  @Column()
  city: string;
}
