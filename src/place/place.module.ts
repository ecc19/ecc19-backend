import {Module} from '@nestjs/common';
import {PlaceController} from './place.controller';
import {PlaceService} from './place.service';
import {ConfigModule} from '@nestjs/config';

@Module({
  controllers: [PlaceController],
  providers: [PlaceService],
  imports: [ConfigModule]
})
export class PlaceModule {}
