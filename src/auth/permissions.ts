export const Permissions = {
  OFFERING_ADMIN: 'offering:list',

  USER_ADMIN: 'user:admin',
  MESSAGE_ADMIN: 'message:admin'
};
