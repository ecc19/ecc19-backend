import {Controller, Get, Query} from '@nestjs/common';
import {ApiTags} from '@nestjs/swagger';
import {PlaceService} from './place.service';
import {PlacesNearbyResponseData} from '@googlemaps/google-maps-services-js/dist/places/placesnearby';

@ApiTags('places')
@Controller('places')
export class PlaceController {
  constructor(
    private placeService: PlaceService
  ) {
  }

  @Get('pharmacies')
  getPharmacies(
    @Query('query') query: string,
  ): Promise<PlacesNearbyResponseData> {
    return this.placeService.getPlaces(query, 'pharmacy');
  }
}
