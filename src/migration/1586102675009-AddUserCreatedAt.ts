import {MigrationInterface, QueryRunner} from "typeorm";

export class AddUserCreatedAt1586102675009 implements MigrationInterface {
    name = 'AddUserCreatedAt1586102675009'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" ADD "createdAt" TIMESTAMP NOT NULL DEFAULT now()`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "user" DROP COLUMN "createdAt"`, undefined);
    }

}
