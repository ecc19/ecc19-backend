import {Injectable} from '@nestjs/common';
import {UserService} from '../user/user.service';
import * as argon2 from 'argon2';
import {UserEntity} from '../user/user.entity';
import {JwtService} from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService
  ) {
  }

  async validateUser(username: string, password: string): Promise<UserEntity> {
    const user = await this.userService.findOne(username);
    if (user && await argon2.verify(user.password, password)) {
      return user;
    }

    return null;
  }

  /**
   * Returns access token
   * @param user
   */
  async login(user: UserEntity): Promise<string> {
    const payload = { username: user.username, sub: user.id };
    return this.jwtService.sign(payload);
  }
}
