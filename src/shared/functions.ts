import * as _ from 'lodash';

export function pruneEmpty(obj) {
  _.forOwn(obj, function (value, key) {
    if (_.isString(value) && _.isEmpty(value)) {
      delete obj[key];
    } else if (_.isArray(value)) {
      value.forEach(el => pruneEmpty(el))
    } else if (_.isObject(value)) {
      pruneEmpty(value)
    }
  });

  return obj;
}

