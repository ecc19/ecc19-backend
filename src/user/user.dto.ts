import {UserEntity} from './user.entity';
import {PaginationDto} from '../shared/pagination.dto';
import {ApiProperty} from '@nestjs/swagger';

export class UserPaginationDto extends PaginationDto<UserEntity> {
  @ApiProperty({type: [UserEntity]})
  readonly items: UserEntity[];
}
