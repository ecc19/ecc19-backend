import {ApiProperty} from '@nestjs/swagger';
import {UserEntity} from '../user/user.entity';
import {Type} from 'class-transformer';

export class AuthLoginRequest {
  @ApiProperty()
  username: string;

  @ApiProperty()
  password: string;
}

export class AuthLoginResponse {
  @ApiProperty()
  access_token: string;

  @Type(() => UserEntity)
  @ApiProperty()
  user: UserEntity
}
