import {Module} from '@nestjs/common';
import {MessageController} from './message.controller';
import {MessageService} from './message.service';
import {PassportModule} from '@nestjs/passport';
import {TypeOrmModule} from '@nestjs/typeorm';
import {MessageEntity} from './message.entity';
import {EmailModule} from '../email/email.module';
import {ConfigModule} from '@nestjs/config';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([MessageEntity]),
    EmailModule,
    ConfigModule
  ],
  controllers: [MessageController],
  providers: [MessageService]
})
export class MessageModule {}
