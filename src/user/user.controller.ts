import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors,
  UsePipes,
  ValidationPipe
} from '@nestjs/common';
import {UserEntity} from './user.entity';
import {ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags} from '@nestjs/swagger';
import {AuthGuard} from '@nestjs/passport';
import {PermissionGuard} from '../auth/guard/permission-guard.service';
import {Restrict} from '../auth/decorator/restrict.decorator';
import {Permissions} from '../auth/permissions';
import {UserService} from './user.service';
import {Pagination} from 'nestjs-typeorm-paginate';
import {UserPaginationDto} from './user.dto';

@ApiTags('users')
@ApiBearerAuth()
@UseGuards(AuthGuard(), PermissionGuard)
@UseInterceptors(ClassSerializerInterceptor)
@Controller('users')
export class UserController {
  constructor(
    private userService: UserService
  ) {
  }

  @ApiOperation({ summary: 'Get all users' })
  @ApiResponse({ status: 200, description: 'Return all users.', type: UserPaginationDto})
  @ApiQuery({ name: 'page', required: false})
  @ApiQuery({ name: 'limit', required: false})
  @Restrict(Permissions.USER_ADMIN)
  @Get()
  findAll(
    @Query('page') page: number = 0,
    @Query('limit') limit: number = 10
  ): Promise<Pagination<UserEntity>> {
    limit = limit > 100 ? 100 : limit;
    return this.userService.findAll({page, limit});
  }

  @ApiOperation({ summary: 'Get one user' })
  @ApiResponse({ status: 200, description: 'Return the user'})
  @Restrict(Permissions.USER_ADMIN)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<UserEntity> {
    return this.userService.findOne(id);
  }

  @ApiOperation({ summary: 'Create new user' })
  @ApiResponse({ status: 201, description: 'The user has been successfully created.'})
  @ApiResponse({ status: 409, description: 'Conflict.' })
  @UsePipes(new ValidationPipe({transform: true}))
  @Restrict(Permissions.USER_ADMIN)
  @Post('')
  create(@Body() userData: UserEntity): Promise<UserEntity> {
    return this.userService.create(userData);
  }

  @ApiOperation({ summary: 'Update user' })
  @ApiResponse({ status: 201, description: 'The user has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Restrict(Permissions.USER_ADMIN)
  @Put(':id')
  update(@Param('id') id: number, @Body() user: UserEntity): Promise<UserEntity> {
    return this.userService.update(id, user)
  }
}

