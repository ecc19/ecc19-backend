import {ClassSerializerInterceptor, Controller, Post, UploadedFile, UseInterceptors} from '@nestjs/common';
import {FileInterceptor} from '@nestjs/platform-express';
import {FileService} from './file.service';
import {FileDataEntity} from './file-data.entity';
import {ApiConsumes, ApiOperation, ApiResponse, ApiTags} from '@nestjs/swagger';
import {ApiFile} from '../shared/api-file.decorator';

@UseInterceptors(ClassSerializerInterceptor)
@ApiTags('files')
@Controller('files')
export class FileController {
  constructor(
    private fileService: FileService
  ) {
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload file' })
  @ApiConsumes('multipart/form-data')
  @ApiFile('file')
  @ApiResponse({status: 201, type: FileDataEntity})
  uploadFile(@UploadedFile() file): Promise<FileDataEntity> {
    return this.fileService.create(file);
  }
}
