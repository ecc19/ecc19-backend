import {BeforeInsert, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {IsEmail, IsNotEmpty} from 'class-validator';
import * as argon2 from 'argon2';
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';
import {Exclude} from 'class-transformer';

@Entity('user')
export class UserEntity {

  @Exclude({toClassOnly: true})
  @ApiPropertyOptional()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Exclude({toClassOnly: true})
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @Column()
  @IsNotEmpty()
  username: string;

  @ApiProperty()
  @Column()
  @IsEmail()
  email: string;

  @Exclude({toPlainOnly: true})
  @ApiProperty()
  @Column()
  @IsNotEmpty()
  password: string;

  /**
   * Roles as comma separated list
   */
  @ApiPropertyOptional()
  @Column({default: ''})
  roles: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await argon2.hash(this.password);
  }
}
