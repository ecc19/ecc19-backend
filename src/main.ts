import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {INestApplication} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {AppService} from './app.service';
import * as helmet from 'helmet';
import * as rateLimit from 'express-rate-limit';
import {NestExpressApplication} from '@nestjs/platform-express';


function initSecurity(app: NestExpressApplication) {
  app.enableCors();
  app.use(helmet());

  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  app.set('trust proxy', 1)
}

function initSwagger(app: INestApplication) {
  const appService = app.get(AppService);
  const appInfo = appService.getAppInfo();
  const options = new DocumentBuilder()
    .setTitle('Ensemble contre le Covid 19')
    .setDescription(appInfo.description)
    .setVersion(appInfo.version)
    .addBasicAuth()
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);
}

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    // logger: ['log', 'error', 'warn', 'debug', 'verbose'],
  });

  initSecurity(app);
  initSwagger(app);

  const configService = app.get(ConfigService);
  await app.listen(configService.get('server.port'));
}

bootstrap();
