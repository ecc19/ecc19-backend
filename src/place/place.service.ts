import {Injectable} from '@nestjs/common';
import {Client} from "@googlemaps/google-maps-services-js";
import {ConfigService} from '@nestjs/config';
import {PlacesNearbyResponseData} from '@googlemaps/google-maps-services-js/dist/places/placesnearby';

@Injectable()
export class PlaceService {
  private readonly client: Client;
  private readonly key: string;

  constructor(
    configService: ConfigService
  ) {
    this.client = new Client({});
    this.key = configService.get<string>('google.apiKey');
  }

  async getPlaces(latLng: string, type: string): Promise<PlacesNearbyResponseData> {
    const result = await this.client.placesNearby({
      params: {
        location: latLng,
        rankby: 'distance',
        key: this.key,
        type: type
      }
    });
    return result.data;
  }
}
