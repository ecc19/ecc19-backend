import {Column, CreateDateColumn, Entity, PrimaryColumn} from 'typeorm';
import {IsDate} from 'class-validator';
import {ApiProperty} from '@nestjs/swagger';
import {Exclude} from 'class-transformer';

@Entity('file_data')
export class FileDataEntity {

  @ApiProperty()
  @Exclude({toClassOnly: true})
  @IsDate()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @PrimaryColumn()
  url: string;

  @ApiProperty()
  @Column()
  originalName: string;

  @ApiProperty()
  @Column()
  mimeType: string;

  @ApiProperty()
  @Column()
  size: number;
}
