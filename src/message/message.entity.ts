import {Column, CreateDateColumn, Entity, PrimaryGeneratedColumn} from 'typeorm';
import {ApiProperty} from '@nestjs/swagger';
import {Exclude} from 'class-transformer';

@Entity('message')
export class MessageEntity {

  @Exclude({toClassOnly: true})
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Exclude({toClassOnly: true})
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty()
  @Column()
  subject: string;

  @ApiProperty()
  @Column()
  name: string;

  @ApiProperty()
  @Column()
  email: string;

  @ApiProperty()
  @Column()
  content: string;
}
