import {SetMetadata} from '@nestjs/common';

export const Restrict = (...args: string[]) => SetMetadata('permissions', args);
