import {Permissions} from './permissions';

export const Roles = {
  admin: {
    name: 'admin', permissions: [
      Permissions.USER_ADMIN,
      Permissions.MESSAGE_ADMIN,
      Permissions.OFFERING_ADMIN,
    ]
  }
};
