import {Injectable} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(
    private configService: ConfigService
  ) {
  }
  getAppInfo(): { name: string; description: string; version: string } {
    return {
      name: this.configService.get('app.name'),
      version: this.configService.get('app.version'),
      description: this.configService.get('app.description'),
    };
  }
}
