import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import {OfferingService} from './offering.service';
import {ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags} from '@nestjs/swagger';
import {OfferingEntity} from './offering.entity';
import {Pagination} from 'nestjs-typeorm-paginate';
import {AuthGuard} from '@nestjs/passport';
import {PermissionGuard} from '../auth/guard/permission-guard.service';
import {Restrict} from '../auth/decorator/restrict.decorator';
import {Permissions} from '../auth/permissions';

@ApiTags('offerings')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('offerings')
export class OfferingController {
  constructor(
    private readonly offeringService: OfferingService
  ) {
  }

  @ApiOperation({ summary: 'Get all offerings' })
  @ApiResponse({ status: 200, description: 'Return all offerings.'})
  @ApiQuery({ name: 'page', required: false})
  @ApiQuery({ name: 'limit', required: false})
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), PermissionGuard)
  @Restrict(Permissions.OFFERING_ADMIN)
  @Get()
  async findAll(
    @Query('page') page: number = 0,
    @Query('limit') limit: number = 10
  ): Promise<Pagination<OfferingEntity>> {
    limit = limit > 100 ? 100 : limit;
    return await this.offeringService.findAll({page, limit});
  }

  @ApiOperation({ summary: 'Get one offering' })
  @ApiResponse({ status: 200, description: 'Return all articles.'})
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<OfferingEntity> {
    return await this.offeringService.findOne(id);
  }

  @ApiOperation({ summary: 'Create offering' })
  @ApiResponse({ status: 201, description: 'The offering has been successfully created.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Post()
  async create(@Body() offeringData: OfferingEntity) {
    return this.offeringService.create(offeringData);
  }

  @ApiOperation({ summary: 'Update offering' })
  @ApiResponse({ status: 201, description: 'The offering has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @Put(':id')
  update(@Param('id') id: string, @Body() offering: OfferingEntity): Promise<OfferingEntity> {
    return this.offeringService.update(id, offering)
  }
}
