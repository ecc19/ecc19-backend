export enum EquipmentTypeEnum {
  masques = 'masques',
  lunettes = 'lunettes',
  blouses = 'blouses',
  gel = 'gel',
  gants = 'gants',
  pyjamas = 'pyjamas',
  charlottes = 'charlottes',
  surblouses = 'surblouses',
  surchaussures = 'surchaussures',
  salopettes = 'salopettes',
  bonnets = 'bonnets',
  frottis = 'frottis',
  desinfectantsol = 'desinfectantsol',
  desinfectantsurface = 'desinfectantsurface',
  autre = 'autre'
}
