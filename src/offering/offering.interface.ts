import {OfferingEntity} from './offering.entity';

export interface OfferingListResponse {
  elements: OfferingEntity[];
  count: number;
}

export interface CreateOfferingRequest {

}
