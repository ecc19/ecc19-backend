import {MessageEntity} from './message.entity';
import {PaginationDto} from '../shared/pagination.dto';
import {ApiProperty} from '@nestjs/swagger';

export class MessagePaginationDto extends PaginationDto<MessageEntity> {
  @ApiProperty({type: [MessageEntity]})
  readonly items: MessageEntity[];
}
