import {ClassSerializerInterceptor, Controller, Get, Post, Request, UseGuards, UseInterceptors} from '@nestjs/common';
import {LocalAuthGuard} from './guard/local-auth.guard';
import {ApiBearerAuth, ApiBody, ApiResponse, ApiTags} from '@nestjs/swagger';
import {UserEntity} from '../user/user.entity';
import {UserService} from '../user/user.service';
import {AuthService} from './auth.service';
import {AuthLoginRequest, AuthLoginResponse} from './auth.dto';
import {AuthGuard} from '@nestjs/passport';
import {plainToClass} from 'class-transformer';

@ApiTags('auth')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('auth')
export class AuthController {
  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  @UseGuards(LocalAuthGuard)
  @ApiBody({type: AuthLoginRequest})
  @ApiResponse({status: 200, type: AuthLoginResponse})
  @Post('login')
  async login(@Request() req): Promise<AuthLoginResponse> {
    // take advantage of class transformer
    return plainToClass(AuthLoginResponse, {
      access_token: await this.authService.login(req.user),
      user: req.user
    });
  }

  @UseGuards(AuthGuard())
  @ApiBearerAuth()
  @ApiResponse({type: UserEntity, status: 200})
  @Get('me')
  getMe(@Request() req): UserEntity {
    return req.user;
  }
}
