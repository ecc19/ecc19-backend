import {Module} from '@nestjs/common';
import {FileService} from './file.service';
import {FileController} from './file.controller';
import {ConfigModule} from '@nestjs/config';
import {MulterModule} from '@nestjs/platform-express';
import {MulterConfigService} from './multer-config.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {FileDataEntity} from './file-data.entity';

@Module({
  imports: [
    ConfigModule,
    MulterModule.registerAsync({
      imports: [ConfigModule],
      useClass: MulterConfigService,
    }),
    TypeOrmModule.forFeature([FileDataEntity])
  ],
  providers: [FileService, MulterConfigService],
  controllers: [FileController]
})
export class FileModule {}
