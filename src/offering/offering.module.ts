import {Module} from '@nestjs/common';
import {OfferingService} from './offering.service';
import {OfferingController} from './offering.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {OfferingEntity} from './offering.entity';
import {OfferingItemEntity} from './offering-item.entity';
import {FileModule} from '../file/file.module';
import {PassportModule} from '@nestjs/passport';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt' }),
    TypeOrmModule.forFeature([OfferingEntity, OfferingItemEntity]), FileModule
  ],
  providers: [OfferingService],
  controllers: [OfferingController]
})
export class OfferingModule {}
