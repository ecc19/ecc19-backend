import {Column, CreateDateColumn, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn} from 'typeorm';
import {OfferingItemEntity} from './offering-item.entity';
import {IsDate, IsEmail, IsNumber, IsPhoneNumber, IsString} from 'class-validator';
import {Exclude} from 'class-transformer';
import {ApiProperty, ApiPropertyOptional} from '@nestjs/swagger';

@Entity('offering')
export class OfferingEntity {
  @ApiProperty()
  @PrimaryColumn()
  @Exclude({toClassOnly: true})
  id: string;

  @ApiProperty()
  @Exclude({toClassOnly: true})
  @IsDate()
  @CreateDateColumn()
  createdAt: Date;

  @ApiProperty({type: [OfferingItemEntity]})
  @OneToMany(type => OfferingItemEntity, item => item.offering, {cascade: true})
  items: OfferingItemEntity[];

  @ApiProperty()
  @IsString()
  @Column()
  name: string;

  @ApiPropertyOptional()
  @IsString()
  @Column({nullable: true})
  company: string;

  @ApiProperty()
  @IsEmail()
  @Column()
  email: string;

  @ApiProperty()
  @IsPhoneNumber('FR')
  @Column()
  phone: string;
}
