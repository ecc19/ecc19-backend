export default () => ({
  app: {
    name: process.env.npm_package_name,
    version: process.env.npm_package_version,
    description: process.env.npm_package_description,
  },

  server: {
    port: parseInt(process.env.SERVER_PORT, 10) || 3000,
  },

  database: {
    type: process.env.ORM_TYPE || 'postgres',
    host: process.env.ORM_HOST || 'localhost',
    port: parseInt(process.env.ORM_PORT, 10) || 5432,
    username: process.env.ORM_USERNAME,
    password: process.env.ORM_PASSWORD,
    database: process.env.ORM_DATABASE,
    entities: ["dist/**/*.entity.js"],
    synchronize: process.env.ORM_SYNCHRONIZE == 'true'
  },

  jwt: {
    secret: process.env.JWT_SECRET,
    signOptions: {
      expiresIn: process.env.JWT_EXPIRES_IN || '1y',
    },
  },

  aws: {
    bucketName: process.env.AWS_BUCKET_NAME,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sesRegion: process.env.AWS_SES_REGION
  },

  google: {
      apiKey: process.env.GOOGLE_MAPS_API_KEY
  },

  admin: {
    username: process.env.ADMIN_USERNAME,
    password: process.env.ADMIN_PASSWORD,
    email: process.env.ADMIN_EMAIL,
  }
});
