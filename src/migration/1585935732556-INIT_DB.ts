import {MigrationInterface, QueryRunner} from "typeorm";
import * as argon2 from 'argon2';

const dotenv: any = require("dotenv");

export class INITDB1585935732556 implements MigrationInterface {
  name = 'INITDB1585935732556'

  public async up(queryRunner: QueryRunner): Promise<void> {
    dotenv.config();

    // add admin user
    const password = await argon2.hash(process.env.ADMIN_PASSWORD);
    await queryRunner.query(`INSERT INTO "user" ( "username", "email", "password", "roles")
                                    VALUES ('${process.env.ADMIN_USERNAME}', '${process.env.ADMIN_EMAIL}', '${password}', 'admin')`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {

  }

}
