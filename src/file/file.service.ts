import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {FileDataEntity} from './file-data.entity';

@Injectable()
export class FileService {
  constructor(
    @InjectRepository(FileDataEntity)
    private readonly fileRepository: Repository<FileDataEntity>,
  ) {
  }

  create(file: any): Promise<FileDataEntity> {
    console.log(file);

    const fileData = new FileDataEntity();
    fileData.url = file.location;
    fileData.originalName = file.originalname.replace(/\u0000/g,'');
    fileData.mimeType = file.mimetype;
    fileData.size = file.size;
    return this.fileRepository.save(fileData);
  }
}
