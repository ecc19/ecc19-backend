import {BadRequestException, Injectable} from '@nestjs/common';
import {MulterOptionsFactory} from '@nestjs/platform-express';
import * as AWS from 'aws-sdk';
import * as multerS3 from 'multer-s3';
import {ConfigService} from '@nestjs/config';
import {MulterOptions} from '@nestjs/platform-express/multer/interfaces/multer-options.interface';
import {Strings} from '../shared/strings';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  constructor(
    private configService: ConfigService
  ) {
    AWS.config.update({
      accessKeyId: this.configService.get<string>('aws.accessKeyId'),
      secretAccessKey: this.configService.get<string>('aws.secretAccessKey'),
    });
  }

  public createMulterOptions(): MulterOptions {
    const bucketName = this.configService.get<string>('aws.bucketName');
    const s3 = new AWS.S3();

    return {
      storage: multerS3({
        s3: s3,
        bucket: bucketName,
        acl: 'public-read',
        metadata: function (req, file, cb) {
          cb(null, {fieldName: file.fieldname});
        },
        key: function (req, file, cb) {
          cb(null, `${Date.now().toString()}_${Strings.sanitizeForKey(file.originalname)}`);
        }
      }),
      limits: {
        fileSize: 5 * 1024 * 1024,
        files: 5
      },
      fileFilter: (req, file, cb) => {
        if (file.mimetype.match('image.*')) {
          cb(null, true);
        } else {
          cb(new BadRequestException('Only images allowed'), false);
        }
      }
    }
  }
}
