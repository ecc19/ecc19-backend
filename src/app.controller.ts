import {ClassSerializerInterceptor, Controller, Get, UseInterceptors} from '@nestjs/common';
import {AppService} from './app.service';

@UseInterceptors(ClassSerializerInterceptor)
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService
  ) {}

  @Get()
  getHello(): any {
    return this.appService.getAppInfo();
  }
}
