import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {OfferingEntity} from './offering.entity';
import {Repository} from 'typeorm';
import {v4 as uuidv4} from 'uuid';
import {IPaginationOptions, paginate, Pagination} from 'nestjs-typeorm-paginate';
import {pruneEmpty} from '../shared/functions';

@Injectable()
export class OfferingService {
  constructor(
    @InjectRepository(OfferingEntity)
    private readonly offeringRepository: Repository<OfferingEntity>,
  ) {
  }

  public async findAll(options: IPaginationOptions): Promise<Pagination<OfferingEntity>> {
    const qb = await this.offeringRepository
      .createQueryBuilder('offering')
      .leftJoinAndSelect('offering.items', 'item');

    qb.orderBy('offering.createdAt', 'DESC');

    return await paginate<OfferingEntity>(qb, options);
  }

  public async create(offeringData: OfferingEntity): Promise<OfferingEntity> {
    offeringData.id = uuidv4();
    pruneEmpty(offeringData); // -> dates should not be empty
    const newOffering = await this.offeringRepository.save(offeringData);
    return newOffering;
  }

  public async update(id: string, offering: OfferingEntity): Promise<OfferingEntity> {
    offering.id = id;
    pruneEmpty(offering); // -> dates should not be empty
    return this.offeringRepository.save(offering);
  }

  public async findOne(id: string): Promise<OfferingEntity> {
    return this.offeringRepository.findOne(id, {relations: ['items']});
  }
}
