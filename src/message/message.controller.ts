import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import {ApiBearerAuth, ApiOperation, ApiQuery, ApiResponse, ApiTags} from '@nestjs/swagger';
import {Restrict} from '../auth/decorator/restrict.decorator';
import {Permissions} from '../auth/permissions';
import {Pagination} from 'nestjs-typeorm-paginate';
import {MessageService} from './message.service';
import {MessagePaginationDto} from './message.dto';
import {MessageEntity} from './message.entity';
import {AuthGuard} from '@nestjs/passport';
import {PermissionGuard} from '../auth/guard/permission-guard.service';

@ApiTags('messages')
@UseInterceptors(ClassSerializerInterceptor)
@Controller('messages')
export class MessageController {
  constructor(
    private messageService: MessageService
  ) {
  }

  @ApiOperation({ summary: 'Get all messages' })
  @ApiResponse({ status: 200, description: 'Return all messages.', type: MessagePaginationDto})
  @ApiQuery({ name: 'page', required: false})
  @ApiQuery({ name: 'limit', required: false})
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), PermissionGuard)
  @Restrict(Permissions.MESSAGE_ADMIN)
  @Get()
  findAll(
    @Query('page') page: number = 0,
    @Query('limit') limit: number = 10
  ): Promise<Pagination<MessageEntity>> {
    limit = limit > 100 ? 100 : limit;
    return this.messageService.findAll({page, limit});
  }

  @ApiOperation({ summary: 'Get one message' })
  @ApiResponse({ status: 200, description: 'Return the message'})
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), PermissionGuard)
  @Restrict(Permissions.MESSAGE_ADMIN)
  @Get(':id')
  findOne(@Param('id') id: number): Promise<MessageEntity> {
    return this.messageService.findOne(id);
  }

  @ApiOperation({ summary: 'Create new message' })
  @ApiResponse({ status: 201, description: 'The message has been successfully created.'})
  @ApiResponse({ status: 409, description: 'Conflict.' })
  @Post('')
  create(@Body() messageData: MessageEntity): Promise<MessageEntity> {
    return this.messageService.create(messageData);
  }

  @ApiOperation({ summary: 'Update message' })
  @ApiResponse({ status: 201, description: 'The message has been successfully updated.'})
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  @ApiBearerAuth()
  @UseGuards(AuthGuard(), PermissionGuard)
  @Restrict(Permissions.MESSAGE_ADMIN)
  @Put(':id')
  update(@Param('id') id: number, @Body() message: MessageEntity): Promise<MessageEntity> {
    return this.messageService.update(id, message)
  }
}
