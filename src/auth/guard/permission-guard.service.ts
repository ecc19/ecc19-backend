import {CanActivate, ExecutionContext, Injectable} from '@nestjs/common';
import {Observable} from 'rxjs';
import {Reflector} from '@nestjs/core';
import * as _ from 'lodash';
import {Roles} from '../roles';

@Injectable()
export class PermissionGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const permissions = this.reflector.get<string[]>('permissions', context.getHandler());
    if (!permissions) {
      return true;
    }
    const request = context.switchToHttp().getRequest();
    const user = request.user;
    if (!user.roles) {
      return false;
    }
    return this.matchPermissions(permissions, user.roles.split(','));
  }

  private matchPermissions(permissions: string[], roles: string[]) {
    for (let roleName of roles) {
      const rolePerm = Roles[roleName] ? Roles[roleName].permissions : [];
      if (_.intersection(permissions, rolePerm).length > 0) {
        return true;
      }
    }
    return false;
  }
}
