import {Injectable} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import * as AWS from 'aws-sdk';

@Injectable()
export class EmailService {
  private sesConfig: { accessKeyId: string; secretAccessKey: string; apiVersion: string; region: string };
  private adminEmail: string;

  constructor(
    configService: ConfigService
  ) {
    this.sesConfig = {
      apiVersion: '2010-12-01',
      region: configService.get('aws.sesRegion'),
      accessKeyId: configService.get('aws.accessKeyId'),
      secretAccessKey: configService.get('aws.secretAccessKey'),
    };

    this.adminEmail = configService.get('admin.email');
  }

  public sendEmail(to: string, subject: string, content: string, replyto: string) {
    const params = {
      Destination: { /* required */
        CcAddresses: [],
        ToAddresses: [ to ],
      },
      Source: 'contact@ensemblecontrecovid19.fr', /* required */
      ReplyToAddresses: [
        replyto
      ],
      Message: { /* required */
        Body: { /* required */
          Text: {
            Charset: "UTF-8",
            Data: content
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        }
      },
    };

    // Create the promise and SES service object
    return new AWS.SES(this.sesConfig).sendEmail(params).promise();
  }
}
